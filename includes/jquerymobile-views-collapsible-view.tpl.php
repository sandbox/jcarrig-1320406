<?php
/**
 * @file
 * jquerymobile-views-collapsible-view.tpl.php
 *
 * Displays the template of collapsible content blocks.
 */
?>

<div class="<?php print $jquerymobile_views_collapsible_classes; ?>" <?php print $jquerymobile_views_collapsible_set; ?>>
  <?php foreach ($rows as $id => $row): ?>
    <div class="<?php print $classes[$id]; ?>" <?php print $jquerymobile_views_collapsible_datarole; ?>>
	  <h3><?php print $titles[$id]; ?></h3>
	  <?php print $row; ?>
	</div>
  <?php endforeach; ?>
</div>
