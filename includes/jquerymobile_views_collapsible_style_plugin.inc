<?php

/**
 * @file
 * Contains the jQueryMobile Views Grid style plugin.
 */

/**
 * Style plugin to render each item in an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 */
class jquerymobile_views_collapsible_style_plugin extends views_plugin_style {
  function option_definition() {
    $options = parent::option_definition();
    $options['data-role'] = 'collapsible';
    $options['accordion'] = FALSE;
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['data-role'] = array(
      '#type' => 'hidden',
      '#value' => 'collapsible',
    );

    if ($this->uses_fields()) {
      $form['h3'] = array(
        '#type' => 'select',
        '#title' => t('Content block header'),
        '#options' => $this->display->handler->get_field_labels(),
        '#default_value' => $this->options['type'],
      );
    }
    $form['accordion'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add accoridion behavior to the collapsible set.'),
      '#default_value' => $this->options['accordion'],
      '#return_value' => TRUE,
    );
  }

  function validate() {
    $errors = parent::validate();
    return $errors;
  }

}
