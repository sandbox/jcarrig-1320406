<?php

/**
 * @file
 * Contains the jQueryMobile Views List View style plugin.
 */

/**
 * Style plugin to render each item in an jquery mobile list view.
 *
 * @ingroup views_style_plugins
 */
class jquerymobile_views_listview_style_plugin extends views_plugin_style {
  function option_definition() {
    $options = parent::option_definition();
    $options['data-role'] = 'listview';
    $options['type'] = array('default' => 'ul');
    $options['data-theme'] = array('d' => 'white');
    $options['data-inset'] = array('insert' => 'True');
    $options['data-filter'] = FALSE;
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['data-role'] = array(
      '#type' => 'hidden',
      '#value' => 'listview',
    );
    $form['type'] = array(
      '#type' => 'radios',
      '#title' => t('List type'),
      '#options' => array('ul' => t('Unordered list'), 'ol' => t('Ordered list')),
      '#default_value' => $this->options['type'],
    );
    $form['data-theme'] = array(
      '#type' => 'select',
      '#title' => t('Theme'),
      '#default_value' => $this->options['data-theme'],
      '#description' => t('Specifies the list view theme.'),
      '#options' => array(
        'a' => t('black'),
        'b' => t('blue'),
        'c' => t('grey'),
        'd' => t('white'),
        'e' => t('yellow'),
      ),
    );

    $form['data-inset'] = array(
      '#type' => 'select',
      '#title' => t('Inset'),
      '#default_value' => $this->options['data-inset'],
      '#description' => t('Specifies the list view theme.'),
      '#options' => array(
        'inset' => t('True'),
        '' => t('False'),
      ),
    );

    $form['data-filter'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add search filter bar'),
      '#default_value' => $this->options['data-filter'],
      '#return_value' => TRUE,
    );

  }

  function validate() {
    $errors = parent::validate();
    return $errors;
  }

}
