<?php
/**
 * @file
 * jquerymobile-views-grid-view.tpl.php
 *
 * Displays the template of a jquery mobile ui-grid.
 */
?>

<div class="<?php print $jquerymobile_views_grid_classes; ?>">
  <?php foreach ($rows as $id => $row): ?>
    <div class="<?php print $classes[$id]; ?>"><?php print $row; ?></div>
  <?php endforeach; ?>
</div>
