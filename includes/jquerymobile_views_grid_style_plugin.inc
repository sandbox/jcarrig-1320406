<?php

/**
 * @file
 * Contains the jQueryMobile Views Grid style plugin.
 */

/**
 * Style plugin to render each item in an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 */
class jquerymobile_views_grid_style_plugin extends views_plugin_style {
  function option_definition() {
    $options = parent::option_definition();
    $options['numberOfColumns'] = array('a' => 2);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['numberOfColumns'] = array(
      '#type' => 'select',
      '#title' => t('Number of columns'),
      '#default_value' => $this->options['numberOfColumns'],
      '#description' => t('Specifies the number of columns in the layout grid.'),
      '#options' => array(
        '2' => t('2'),
        '3' => t('3'),
        '4' => t('4'),
        '5' => t('5'),
      ),
    );
  }

  function validate() {
    $errors = parent::validate();
    return $errors;
  }

}
