<?php
/**
 * @file
 * Hooks into views plugins and also themes the different 
 * style plugins with pre_process functions. 
 *
 */

/**
 * Implementation of hook_views_plugins().
 */
function jquerymobile_views_views_plugins() {
  return array(
    'style' => array(
      'jquerymobile_views_grid' => array(
        'title' => t('Jquery Mobile Grid'),
        'help' => t('Displays content in a jQuery mobile friendly layout grid.'),
        'handler' => 'jquerymobile_views_grid_style_plugin',
        'path' => drupal_get_path('module', 'jquerymobile_views') . '/includes',
        'theme' => 'jquerymobile_views_grid_view',
        'theme path' => drupal_get_path('module', 'jquerymobile_views') . '/includes',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
      ),
      'jquerymobile_views_listview' => array(
        'title' => t('Jquery Mobile List View'),
        'help' => t('Displays content in a jQuery mobile friendly list view.'),
        'handler' => 'jquerymobile_views_listview_style_plugin',
        'path' => drupal_get_path('module', 'jquerymobile_views') . '/includes',
        'theme' => 'jquerymobile_views_listview_view',
        'theme path' => drupal_get_path('module', 'jquerymobile_views') . '/includes',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => TRUE,
        'type' => 'normal',
      ),
      'jquerymobile_views_collapsible' => array(
        'title' => t('Jquery Mobile Collapsible'),
        'help' => t('Displays content in jQuery mobile friendly collapsible content blocks.'),
        'handler' => 'jquerymobile_views_collapsible_style_plugin',
        'path' => drupal_get_path('module', 'jquerymobile_views') . '/includes',
        'theme' => 'jquerymobile_views_collapsible_view',
        'theme path' => drupal_get_path('module', 'jquerymobile_views') . '/includes',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
      ),
    ),
  );
}

function template_preprocess_jquerymobile_views_grid_view(&$variables) {
  $view = $variables['view'];
  $display_id = empty($view->current_display) ? 'default' : $view->current_display;

  // Translate grid label to number of rows
  $grid_columns = array(
    '0' => 'a',
    '1' => 'b',
    '2' => 'c',
    '3' => 'd',
    '4' => 'e',
  );
  
  // Build the list of classes for the ui-grid.
  $options = $view->style_options;
  $variables['jquerymobile_views_grid_classes_array'] = array(
    'jquerymobile_views_grid',
    views_css_safe('jquerymobile-views-grid-view--' . $view->name . '--' . $display_id),
  );
  if ($options['numberOfColumns']) {
    $variables['jquerymobile_views_grid_classes_array'][] = 'ui-grid-' . $grid_columns[($options['numberOfColumns'] - 2)] ;
  }
  $variables['jquerymobile_views_grid_classes'] = implode(' ', $variables['jquerymobile_views_grid_classes_array']);

  // Views 2/3 compatibility.
  $pager_offset = isset($view->pager['offset']) ? $view->pager['offset'] : $view->offset;

  
  // Give each item a class to identify where in the ui-block it belongs.
  foreach ($variables['rows'] as $id => $row) {
    $variables['classes'][$id] = 'ui-block-' . $grid_columns[($id % $options['numberOfColumns'])];
  }

}


function template_preprocess_jquerymobile_views_listview_view(&$variables) {
  
  
  //dpm($variables);
  
 // if($variables['id']
  
  $view = $variables['view'];
  $display_id = empty($view->current_display) ? 'default' : $view->current_display;
  
  // Build the list of classes for the ui-grid.
  $options = $view->style_options;
  $variables['jquerymobile_views_listview_classes_array'] = array(
    'jquerymobile_views_listview',
    views_css_safe('jquerymobile-views-listview-view--' . $view->name . '--' . $display_id),
  );
  
  
  //if ($options['data-theme']) {
  //  $variables['jquerymobile_views_listview_datatheme'] = ' data-theme="'.$options['data-theme'].'"';
  //}
  
  $variables['jquerymobile_views_listview_attributes'] = '';
  foreach ($options as $k => $v) {
    if ($v) {
      $variables['jquerymobile_views_listview_attributes'] .= ' ' . $k . '="' . $v . '"';
    }
  }
  $variables['jquerymobile_views_listview_classes'] = implode(' ', $variables['jquerymobile_views_listview_classes_array']);

  // Views 2/3 compatibility.
  $pager_offset = isset($view->pager['offset']) ? $view->pager['offset'] : $view->offset;

  
  
  
  // Give each item a class to identify where in the ui-block it belongs.
  foreach ($variables['rows'] as $id => $row) {
    //die(print_r($row));
    $variables['rows'][$id] = strip_tags($row, '<p><a><img><h1><h2><h3><h4>');
    $variables['classes'][$id] = 'test';
  }

}


function template_preprocess_jquerymobile_views_collapsible_view(&$variables) {
  $view = $variables['view'];
  $display_id = empty($view->current_display) ? 'default' : $view->current_display;

  // Build the list of classes for the ui-grid.
  $options = $view->style_options;
  $variables['jquerymobile_views_collapsible_classes_array'] = array(
    'jquerymobile_views_collapsible',
    views_css_safe('jquerymobile-views-collapsible-view--' . $view->name . '--' . $display_id),
  );
  //if ($options['numberOfColumns']) {
  //  $variables['jquerymobile_views_grid_classes_array'][] = 'ui-grid-' . $gridColumns[($options['numberOfColumns'] - 2)] ;
  //}
  $variables['jquerymobile_views_collapsible_classes'] = implode(' ', $variables['jquerymobile_views_collapsible_classes_array']);

  // Views 2/3 compatibility.
  $pager_offset = isset($view->pager['offset']) ? $view->pager['offset'] : $view->offset;

  
  // Give each item a class to identify where in the ui-block it belongs.
  foreach ($variables['rows'] as $id => $row) {
    //$variables['classes'][$id] = 'item-' . $gridColumns[($id % $options['numberOfColumns'])];
    $variables['classes'][$id] = "test";
    //$variables['rows'][$id] = strip_tags($row,'<p><a><img><h1><h2><h3><h4>');
    $variables['titles'][$id] = $view->style_plugin->get_field($id, $options['h3']);
  }
  if ($options['accordion']) {
    $variables['jquerymobile_views_collapsible_set'] = ' data-role="collapsible-set"';
  }
  $variables['jquerymobile_views_collapsible_datarole'] = ' data-role="' . $options['data-role'] . '"';
  // dpm($variables);
  
}


