<?php
/**
 * @file
 * jquerymobile-views-listview-view.tpl.php
 *
 * Displays the template of a jquery mobile list view.
 */
?>

<<?php print $options['type']; ?> class="<?php print $jquerymobile_views_listview_classes; ?>"<?php print $jquerymobile_views_listview_attributes;?>>
  <?php if (!empty($title)) : ?>
    <li data-role="list-divider"><?php print $title; ?></li>
  <?php endif; ?>
  <?php foreach ($rows as $id => $row): ?>
    <li class="<?php print $classes[$id]; ?>" data-icon="arrow-r"><?php print $row; ?></li>
  <?php endforeach; ?>
</<?php print $options['type']; ?>>
